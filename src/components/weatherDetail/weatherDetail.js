import React from "react";
import "./styles/styles.css";

function WeatherDetail({ city = "", weather = {} }) {
  return (
    <div className="detail-container">
      <div className="content-left">
        <span className="city-title">{city}</span>
        <span><b>{weather.weatherDescription}</b></span>
        <div className="current-temp-container">
          <img
            src={
              weather.weatherIcon &&
              `http://openweathermap.org/img/wn/${weather.weatherIcon}@2x.png`
            }
            alt=""
          />
          <span>{weather.temp}°C</span>
        </div>
      </div>
      <div className="content-right">
        <span>Humidity: <b>{weather.humidity}%</b></span>
        <span>Wind Speed: <b>{weather.windSpeed} Mph</b></span>
        <span>Temp Max: <b>{weather.tempMax}°C</b></span>
        <span>Temp Min: <b>{weather.tempMin}°C</b></span>
        <span>Real feel: <b>{weather.feelsLike}°C</b></span>
      </div>
    </div>
  );
}

export default WeatherDetail;
