import React, { useState } from "react";
import DayWeatherBox from "../../components/dayWeatherBox/dayWeatherBox";
import "./styles/styles.css";

function WeatherBoxes({ weather = {}, callback = () => {} }) {
  return (
    <div className="boxes-container">
      {Object.values(weather).map((item, key) => {
        return (
          <DayWeatherBox
            key={key}
            weather={item}
            callback={(e) => callback(e)}
          />
        );
      })}
    </div>
  );
}

export default WeatherBoxes;
