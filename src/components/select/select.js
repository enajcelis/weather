import React from "react";
import "./styles/styles.css";

function Select({ data = [], callback = () => {} }) {
  return (
    <div>
      <select onChange={(e) => callback(e)} defaultValue="default">
        <option value="default" disabled hidden>
          Select your city
        </option>
        {
          data.map((item, key) => {
            return <option key={key} value={item.value}>{item.title}</option>         
          })
        }
      </select>
    </div>
  );
}

export default Select;
