import React from "react";
import "./styles/styles.css";

function DayWeatherBox({ weather = {}, callback = () => {} }) {
  return (
    <div className="box-container" data-id={weather.date} onClick={(e) => callback(e)}>
      <span>{weather.date}</span>
      <div>
        <img
          src={
            weather.weatherIcon &&
            `http://openweathermap.org/img/wn/${weather.weatherIcon}.png`
          }
          alt=""
        />
      </div>  
      <div className="min-max-container">
        <span>{weather.tempMin}°C</span> <span>{weather.tempMax}°C</span>
      </div>
    </div>  
  );
}

export default DayWeatherBox;
