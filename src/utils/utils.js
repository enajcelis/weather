import moment from "moment";
import { KELVIN_CONSTANT } from "./constants";

export const formatAPIObject = (apiList = []) => {
  try {
    let weatherList = [];

    if (!Array.isArray(apiList)) {
      return;
    }

    apiList.map((item) => {
      let date = item.dt_txt.split(" ");
      date = moment(date[0]).format("DD/MM/YYYY");

      let weatherItem = {
        date: date,
        humidity: item.main.humidity,
        windSpeed: item.wind.speed,
        temp: convertToCelsius(item.main.temp),
        tempMax: convertToCelsius(item.main.temp_max),
        tempMin: convertToCelsius(item.main.temp_min),
        feelsLike: convertToCelsius(item.main.feels_like),
        weatherIcon: item.weather[0].icon,
        weatherDescription: item.weather[0].description,
        weatherMain: item.weather[0].main,
      };

      if (!weatherList[date]) {
        weatherList[date] = weatherItem;
      }

      return true;
    });

    return weatherList;
  } catch (error) {
    console.log(error);

    return error;
  }
};

const convertToCelsius = (kelvin) => {
  return Math.trunc(kelvin - KELVIN_CONSTANT);
};
