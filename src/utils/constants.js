export const CITIES = [
  {title: 'Buenos Aires', value: 'Buenos Aires'},
  {title: 'Caracas', value: 'Caracas'},
  {title: 'Córdoba', value: 'Córdoba'},
  {title: 'Lima', value: 'Lima'},
  {title: 'Montevideo', value: 'Montevideo'},
  {title: 'Quito', value: 'Quito'},
  {title: 'Santiago', value: 'Santiago'}
]

export const API_KEY = 'dbd1456522188ba65834268baedfc498';

export const KELVIN_CONSTANT = 273.15;