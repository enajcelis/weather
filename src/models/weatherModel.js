import axios from "axios";
import {API_KEY} from '../utils/constants';

class WeatherModel {
  static async getLocationClient() {
    try {
      return await axios.get(`https://ipapi.co/json/`);
    } catch (error) {
      console.log(error);

      return error;
    }
  }

  static async getWeather(city = '') {
    try{
      return await axios.get(`http://api.openweathermap.org/data/2.5/forecast?q=${city}&appid=${API_KEY}`);      
    }catch(error){
      console.log(error);

      return error;
    }
  }
}

export default WeatherModel;