import React, { useState, useEffect } from "react";
import { Spinner } from "react-awesome-spinners";
import moment from "moment";
import WeatherModel from "../../models/weatherModel";
import Select from "../../components/select/select";
import WeatherDetail from "../../components/weatherDetail/weatherDetail";
import WeatherBoxes from "../../components/weatherBoxes/weatherBoxes";
import { CITIES } from "../../utils/constants";
import { formatAPIObject } from "../../utils/utils";
import "./App.css";

function App() {
  const [cityClient, setCityClient] = useState("");
  const [cityWeather, setCityWeather] = useState({});
  const [cityWeatherList, setCityWeatherList] = useState({});
  const [dateDayBox, setDatedayBox] = useState("");
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getCityClient();
  }, []);

  useEffect(() => {
    getCityWeather(cityClient);
  }, [cityClient]);

  useEffect(() => {
    updateWeatherMain(dateDayBox);
  }, [dateDayBox]);

  const getCityClient = async () => {
    let response = await WeatherModel.getLocationClient();

    setCityClient(response.data.city);
  };

  const handleOnChangeCity = (e) => {
    updateStyles();

    let { value } = e.target;

    setCityClient(value);
  };

  const handleOnClickDayWeeatherBox = (e) => {
    updateStyles();

    let element = e.target.closest("div.box-container");
    let date = element.getAttribute("data-id");

    // Actualizamos la clase de la box activa
    element.classList.add("border-box");

    // Actualizamos el estado
    setDatedayBox(date);
  };

  const getCityWeather = async () => {
    try {
      if (cityClient !== "") {
        setLoading(true);

        // Obtenemos el listado del clima del API
        let response = await WeatherModel.getWeather(cityClient);

        // Formateamos la respuesta del API en un objeto reducido
        let weatherList = formatAPIObject(response.data.list);

        // Buscamos fecha actual para mostrar su info
        let today = moment().format("DD/MM/YYYY");
        let currentCityWeather = {};

        if (weatherList[today]) {
          currentCityWeather = weatherList[today];
        } else {
          today = moment(today, "DD/MM/YYYY")
            .add(1, "days")
            .format("DD/MM/YYYY");
          currentCityWeather = weatherList[today];
        }

        // Actualizamos el estado
        setCityWeather(currentCityWeather);
        setCityWeatherList(weatherList);
        setLoading(false);
      }
    } catch (error) {
      console.log(error);

      return error;
    }
  };

  const updateWeatherMain = async () => {
    try {
      if (dateDayBox !== "") {
        // Actualizamos el estado para la fecha
        setCityWeather(cityWeatherList[dateDayBox]);
      }
    } catch (error) {
      console.log(error);

      return error;
    }
  };

  const updateStyles = async () => {
    // Eliminamos la clase de la box activa
    let elements = document.getElementsByClassName("border-box");
    [].forEach.call(elements, function (el) {
      el.classList.remove("border-box");
    });
  };

  return (
    <div className="App">
      <Select data={CITIES} callback={(e) => handleOnChangeCity(e)} />

      {loading ? (
        <div className="spinner-container">
          <Spinner color="#666666" />
        </div>
      ) : (
        <div>
          <WeatherDetail city={cityClient} weather={cityWeather} />
          <WeatherBoxes weather={cityWeatherList} callback={(e) => handleOnClickDayWeeatherBox(e)} />
        </div>
      )} 
           
    </div>
  );
}

export default App;
